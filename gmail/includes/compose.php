<?php
ob_start();
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Mailing Script</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <script src="https://kit.fontawesome.com/c2d76f8ebf.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
        <!-- Nav Bar Starts Here  -->
        <nav class="navbar navbar-expand-lg navbar-dark nav1">
          <div class="container">
          <a class="navbar-brand headertext" href="index.php" style="font-size: 35px;">
              <i class="fab fa-mastodon " style="font-size: 40px;"></i>  
              Mohammed Yasar
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
              <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
                </form>
                
              </ul>
              <i class="fas fa-cog" style="color: white; font-size: 20px;"></i>
              
            </div>
          </nav>  
        </div>    
        <!-- Nav Bar Ends Here  -->



    <!-- CONTENT STARTS HERE  -->   <!-- CONTENT STARTS HERE  -->
    <div class="container " style="padding: 20px;" >
    <div style="background-color: #404040; height:35px; border-radius: 7px;">
        <h5 style="color: white; padding-left:10px; padding-top:5px;">New Message</h5>
      </div>
    <div style="padding: 15px; box-shadow: 0px 0px 15px 0px rgb(0,0,0,1); border-radius: 4px;">
      
        <form action="send.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label for="exampleInputEmail1" style="padding-left: 10px;"><h6>From</h6></label>
          <input type="email" name="from" class="form-control" id="exampleInputEmail1" placeholder="Enter Your Email" aria-describedby="emailHelp">
      </div><form action="send.php" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label for="exampleInputEmail1" style="padding-left: 10px;"><h6>To</h6></label>
      <input class="form-control form-control-lg" id="formFileLg" type="file" accept=".csv" name="file">
      </div>
      <div class="form-group">
          <label for="exampleInputEmail1" style="padding-left: 10px;"><h6>CC</h6></label>
          <input type="email" name="cc" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
      </div>
      <div class="form-group">
        <label for="exampleFormControlTextarea1" style="padding-left: 10px;"><h6>Subject</h6></label>
        <textarea class="form-control" name="subject" id="exampleInputEmail1" rows="1"></textarea>
      </div>

      <div class="form-group">
        <label for="exampleFormControlTextarea1"><h6>Compose Email</h6></label>
        <textarea class="form-control" name="message" id="exampleInputEmail1" rows="5"></textarea>
  </div>



      

      <button type="submit" class="btn btn-primary">Submit</button>
      </form> 
    </div> 
    </div>
    <!-- CONTENT ENDS HERE  -->   <!-- CONTENT ENDS HERE  -->






   
    <!-- Footer Starts Here -->            
    <footer>
      <p >
      Copyright 
      <i class="far fa-copyright"></i> 
      2022 All Rights Reserved Gradtutors Agency.
      </p>
    </footer>
    <!-- FOOTER ENDS Here -->





    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../css/style.css">


  </body>
  </html>
