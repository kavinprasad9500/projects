<!doctype html>
<html lang="en">
  <head>
    <title>Gmail</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@500&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/c2d76f8ebf.js" crossorigin="anonymous"></script>
  </head>
  <body>
        <!-- Nav Bar Starts Here  -->
        <nav class="navbar navbar-expand-lg navbar-dark nav1">
          <div class="container">
          <a class="navbar-brand headertext" href="index.php" style="font-size: 35px;">
              <i class="fab fa-mastodon " style="font-size: 40px;"></i>  
              Mohammed Yasar
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
              <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
                </form>
                
              </ul>
              <i class="fas fa-cog" style="color: white; font-size: 20px;"></i>
              
            </div>
          </nav>  
        </div>    
        <!-- Nav Bar Ends Here  -->

      
        <div class="row" style="padding-top: 15px;">
          <div class="col-sm-2" style="justify-content: center;box-shadow: 1px 0px 0px 0px rgba(0,0,0,0.75); ">
	            <a type="button" href="includes/compose.php" class="btn-light btnCompose btn"> <i class="fas fa-plus" style="margin-right: 5px;color:crimson;
                "></i>Compose </a>
            <ul class="nav" style="padding-left: 30px;padding-top: 20px;padding-bottom: 10px; background-color: #FCE8E6; border-radius: 10px;">
                  <li style="color: crimson; font-size: 20px;"><i class="far fa-file-image" style="margin-right: 5px;"></i> Inbox</li>
            </ul>
            <ul class="nav" style="padding-left: 30px;margin-top: 20px;margin-bottom: 10px;">
              <li style=" font-size: 20px;"><i class="far fa-star" style="margin-right: 5px;"></i>Starred</li>
            </ul>
            <ul class="nav" style="margin-left: 30px;margin-top: 20px;margin-bottom: 10px;">
              <li style=" font-size: 20px;"><i class="far fa-clock" style="margin-right: 5px;"></i>Snoozed</li>
            </ul>
            <ul class="nav" style="margin-left: 30px;margin-top: 20px;margin-bottom: 10px;">
              <li style=" font-size: 20px;"><i class="far fa-share-square" style="margin-right: 5px;"></i>Sent</li>
          </ul>
          <ul class="nav" style="margin-left: 30px;margin-top: 20px;margin-bottom: 10px;">
            <li style=" font-size: 20px;"><i class="fab fa-firstdraft" style="margin-right: 5px;"></i>Drafts</li>
          </ul>
          </div>

          <div class="col-sm-10">
            <div class="row" style="padding-bottom: 10px;padding-left: 20px;box-shadow: 0px 0.5px 0px 0px rgba(68, 68, 68, 0.4); ">
            <div class="dropdown">
              <input class="btn btn-secondary dropdown-toggle" type="checkbox" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">All</a>
                <a class="dropdown-item" href="#">None</a>
                <a class="dropdown-item" href="#">Read</a>
                <a class="dropdown-item" href="#">UnRead</a>
                <a class="dropdown-item" href="#">Started</a>
                <a class="dropdown-item" href="#">UnStarted</a>
              </div>
            </div>
            <a href=""><i style="font-size: 20px; margin-left: 50px;" class="fas fa-redo-alt"></i></a>
            <a href=""><i style="font-size: 20px; margin-left: 50px;" class="fas fa-ellipsis-v"></i></a>
          </div>
          <div class="row" style="padding-bottom: 10px; margin-top: 10px;box-shadow: 0px 0.5px 0px 0px rgba(68, 68, 68, 0.4);">
            <div class="col-sm-2">
              <a href="" style="color: crimson;"><i class="far fa-file-image" style="margin-right: 5px;margin-left: 6px;"></i> Primary</a>
            </div>
            <div class="col-sm-2">
              
              <a href=""><i class="fas fa-user-friends" style="margin-right: 5px;"></i> Social<span class="badge badge-light">50 New</span></a>

            </div>
            <div class="col-sm-2">
              <a href=""><i class="fas fa-tag" style="margin-right: 5px;"></i> Promotions</a>
            </div>
            <div class="col-sm-6">
              
            </div>
          </div>


          <!-- Mail Starts Here  -->      <!-- Mail Starts Here  -->
          <div>
        <div class="row messages" style="margin-top: 10px;box-shadow: 0px 0.5px 0px 0px rgba(0,0,0,0.75);">
          <input style="margin-left: 20px; margin-top: 10px;" type="checkbox">
          <i class="far fa-star" style="margin-right: 20px;margin-top: 8px; margin-left: 20px;"></i>
          <h6 style="color: black; margin-right: 20px;margin-top: 8px;">LinkedIn Job Alerts</h6>
          <p class="subject" style="color: black; margin-right: 20px;margin-top: 5px;">2 new jobs for 'Ios Developer'</p>
          <p style="margin-top: 5px;">Tom, BEE Pro Is Exactly What You Needed 😊 Here’s Why</p>
      </div>
      <div class="row messages" style="margin-top: 10px;box-shadow: 0px 0.5px 0px 0px rgba(0,0,0,0.75);">
        <input style="margin-left: 20px; margin-top: 10px;" type="checkbox">
        <i class="far fa-star" style="margin-right: 20px;margin-top: 8px; margin-left: 20px;"></i>
        <h6 style="color: black; margin-right: 20px;margin-top: 8px;">LinkedIn Job Alerts</h6>
        <p class="subject" style="color: black; margin-right: 20px;margin-top: 5px;">2 new jobs for 'Ios Developer'</p>
        <p style="margin-top: 5px;">Tom, BEE Pro Is Exactly What You Needed 😊 Here’s Why</p>
    </div>
      <div class="row messages" style="margin-top: 10px;box-shadow: 0px 0.5px 0px 0px rgba(0,0,0,0.75);">
        <input style="margin-left: 20px; margin-top: 10px;" type="checkbox">
        <i class="far fa-star" style="margin-right: 20px;margin-top: 8px; margin-left: 20px;"></i>
        <h6 style="color: black; margin-right: 20px;margin-top: 8px;">LinkedIn Job Alerts</h6>
        <p class="subject" style="color: black; margin-right: 20px;margin-top: 5px;">2 new jobs for 'Ios Developer'</p>
        <p style="margin-top: 5px;">Tom, BEE Pro Is Exactly What You Needed 😊 Here’s Why</p>
    </div>
    <div class="row messages" style="margin-top: 10px;box-shadow: 0px 0.5px 0px 0px rgba(0,0,0,0.75);">
      <input style="margin-left: 20px; margin-top: 10px;" type="checkbox">
      <i class="far fa-star" style="margin-right: 20px;margin-top: 8px; margin-left: 20px;"></i>
      <h6 style="color: black; margin-right: 20px;margin-top: 8px;">LinkedIn Job Alerts</h6>
      <p class="subject" style="color: black; margin-right: 20px;margin-top: 5px;">2 new jobs for 'Ios Developer'</p>
      <p style="margin-top: 5px;">Tom, BEE Pro Is Exactly What You Needed 😊 Here’s Why</p>
  </div>
  <div class="row messages" style="margin-top: 10px;box-shadow: 0px 0.5px 0px 0px rgba(0,0,0,0.75);">
    <input style="margin-left: 20px; margin-top: 10px;" type="checkbox">
    <i class="far fa-star" style="margin-right: 20px;margin-top: 8px; margin-left: 20px;"></i>
    <h6 style="color: black; margin-right: 20px;margin-top: 8px;">LinkedIn Job Alerts</h6>
    <p class="subject" style="color: black; margin-right: 20px;margin-top: 5px;">2 new jobs for 'Ios Developer'</p>
    <p style="margin-top: 5px;">Tom, BEE Pro Is Exactly What You Needed 😊 Here’s Why</p>
</div>
<div class="row messages" style="margin-top: 10px;box-shadow: 0px 0.5px 0px 0px rgba(0,0,0,0.75);">
  <input style="margin-left: 20px; margin-top: 10px;" type="checkbox">
  <i class="far fa-star" style="margin-right: 20px;margin-top: 8px; margin-left: 20px;"></i>
  <h6 style="color: black; margin-right: 20px;margin-top: 8px;">LinkedIn Job Alerts</h6>
  <p class="subject" style="color: black; margin-right: 20px;margin-top: 5px;">2 new jobs for 'Ios Developer'</p>
  <p style="margin-top: 5px;">Tom, BEE Pro Is Exactly What You Needed 😊 Here’s Why</p>
</div>
<div class="row messages" style="margin-top: 10px;box-shadow: 0px 0.5px 0px 0px rgba(0,0,0,0.75);">
  <input style="margin-left: 20px; margin-top: 10px;" type="checkbox">
  <i class="far fa-star" style="margin-right: 20px;margin-top: 8px; margin-left: 20px;"></i>
  <h6 style="color: black; margin-right: 20px;margin-top: 8px;">LinkedIn Job Alerts</h6>
  <p class="subject" style="color: black; margin-right: 20px;margin-top: 5px;">2 new jobs for 'Ios Developer'</p>
  <p style="margin-top: 5px;">Tom, BEE Pro Is Exactly What You Needed 😊 Here’s Why</p>
</div>
<div class="row messages" style="margin-top: 10px;box-shadow: 0px 0.5px 0px 0px rgba(0,0,0,0.75);">
  <input style="margin-left: 20px; margin-top: 10px;" type="checkbox">
  <i class="far fa-star" style="margin-right: 20px;margin-top: 8px; margin-left: 20px;"></i>
  <h6 style="color: black; margin-right: 20px;margin-top: 8px;">LinkedIn Job Alerts</h6>
  <p class="subject" style="color: black; margin-right: 20px;margin-top: 5px;">2 new jobs for 'Ios Developer'</p>
  <p style="margin-top: 5px;">Tom, BEE Pro Is Exactly What You Needed 😊 Here’s Why</p>
</div>
<div class="row messages" style="margin-top: 10px;box-shadow: 0px 0.5px 0px 0px rgba(0,0,0,0.75);">
  <input style="margin-left: 20px; margin-top: 10px;" type="checkbox">
  <i class="far fa-star" style="margin-right: 20px;margin-top: 8px; margin-left: 20px;"></i>
  <h6 style="color: black; margin-right: 20px;margin-top: 8px;">LinkedIn Job Alerts</h6>
  <p class="subject" style="color: black; margin-right: 20px;margin-top: 5px;">2 new jobs for 'Ios Developer'</p>
  <p style="margin-top: 5px;">Tom, BEE Pro Is Exactly What You Needed 😊 Here’s Why</p>
</div>
            </div>


          </div>
        </div>
        
     
    <!-- Footer Starts Here -->            
    <footer>
      <p >
      Copyright 
      <i class="far fa-copyright"></i> 
      2022 All Rights Reserved Gradtutors Agency.
      </p>
    </footer>
    <!-- FOOTER ENDS Here -->




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/style.css">
</body>
</html>